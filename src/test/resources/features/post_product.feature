Feature: Search Feature Of Products

  @+verification
  Scenario Outline: Validate searching "<product>"

    When user GET to the "<product>"
    Then Validate the status code <StatusCode>
    And Results should be able to contains "<product>"


    Examples:
      | product | StatusCode |
      | Orange  | 200        |
      | Apple   | 200        |
      | Pasta   | 200        |
      | Cola    | 200        |


  @+verification
  Scenario Outline: Schema Validation "<product>"

    When user GET to the "<product>"
    Then Validate the Schema

    Examples:
      | product |
      | Orange  |
      | Apple   |
      | Pasta   |
      | Cola    |


  @-verification
  Scenario Outline: Validate the "<product>" not be searched

    When user GET to the "<product>"
    Then Validate the status code <StatusCode>
    And The "<product>" should NOT be able to be searched

    Examples:
      | product | StatusCode |
      | water   | 404        |
      | mango   | 404        |
      | tofu    | 404        |
      #false negative | apple   | 404        |





