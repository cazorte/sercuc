package starter;

import java.io.IOException;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
public class Reqspec {
    public static RequestSpecification rspec() throws IOException {

        String baseUrl = configuration.config();
        return new RequestSpecBuilder()
                .setBaseUri(baseUrl)
                .setBasePath("api")
                .setContentType("application/json")
                .build();
    }

}
