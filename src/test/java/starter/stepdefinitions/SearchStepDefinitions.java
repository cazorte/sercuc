package starter.stepdefinitions;

import java.io.IOException;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import starter.Utils;


public class SearchStepDefinitions {

    @Steps
    public Utils anil;

    @When("user GET to the {string}")
    public void search_product_HTTP_Request(String s) throws IOException{
        anil.search_product_request(s);
    }

    @Then("Validate the Schema")
    public void validate_Schema() {
        anil.validate_Schema();
    }

    @Then("Results should be able to contains {string}")
    public void validate_each_result_contains_product(String p) {
        anil.validate_result_contain_product(p);
    }

    @Then("The {string} should NOT be able to be searched")
    public void validate_fail_search(String p) {
        anil.validate_fail_search(p);
    }

    @Then("Validate the status code {int}")
    public void validate_HTTP_status_code(int stcode) {
        anil.validate_status_code(stcode);
    }

}



















