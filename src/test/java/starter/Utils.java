package starter;


import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static net.serenitybdd.rest.SerenityRest.then;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;



public class Utils {


    @Step
    public Response search_product_request(String product) throws IOException {

        // calling URL from RequestSpecification and using path parameters with product arg.
        return SerenityRest.given().log().uri().spec(Reqspec.rspec()).pathParam("product",product.toLowerCase() ).get("v1/search/demo/{product}");
    }

    @Step
    public void validate_result_contain_product(String keyword) {

        // collect API response as a key=value format in List
        List<HashMap<String, Object>> list = lastResponse().jsonPath().get("$");

        ArrayList<String> searchResult = new ArrayList<>();

        // collect "title" fields from list
        for (HashMap<String, Object> stringObjectHashMap : list) {
            searchResult.add((String) stringObjectHashMap.get("title"));
        }

        // validation contain the search keyword
        searchResult.forEach(oneSearchResult -> assertThat(oneSearchResult.toLowerCase()).contains(keyword.toLowerCase()));

    }

    @Step
    public void validate_fail_search(String product) {

        validate_status_code(404);

        JsonPath response= lastResponse().jsonPath();
        assertEquals("Assertion Fail--> ", "Not found", response.get("detail.message"));
    }

    @Step
    public void validate_status_code(int statusCode) {
        restAssuredThat(response -> response.statusCode(statusCode));
    }

    @Step
    public void validate_Schema() {
        then().body(matchesJsonSchemaInClasspath("schema.json"));
    }
}